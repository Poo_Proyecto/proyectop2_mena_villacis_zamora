/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author jonat
 */
public class Vehiculo implements Serializable{
    private static final long serialVersionUID= 78782626363637L;

    protected String placa;
    protected String marca;
    protected String modelo;
    protected String tipo_motor;
    protected String año;
    protected int recorrido;
    protected String color;
    protected String tipo_combustible;
    protected int vidrios;
    protected String transmision;
    protected double precio;
    protected String tipo;
    protected String imagen;
    protected String correoDueno;

    public Vehiculo(String placa,String correoDueno, String marca, String modelo, String tipo_motor, String año, int recorrido, String color, String tipo_combustible, int vidrios, String transmision, double precio, String tipo,String imagen) {
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.tipo_motor = tipo_motor;
        this.año = año;
        this.recorrido = recorrido;
        this.color = color;
        this.tipo_combustible = tipo_combustible;
        this.vidrios = vidrios;
        this.transmision = transmision;
        this.precio = precio;
        this.tipo = tipo;
        this.imagen=imagen;
        this.correoDueno = correoDueno;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTipo_motor() {
        return tipo_motor;
    }

    public void setTipo_motor(String tipo_motor) {
        this.tipo_motor = tipo_motor;
    }

    public String getAño() {
        return año;
    }

    public void setAño(String año) {
        this.año = año;
    }

    public int getRecorrido() {
        return recorrido;
    }

    public void setRecorrido(int recorrido) {
        this.recorrido = recorrido;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTipo_combustible() {
        return tipo_combustible;
    }

    public void setTipo_combustible(String tipo_combustible) {
        this.tipo_combustible = tipo_combustible;
    }

    public int getVidrios() {
        return vidrios;
    }

    public void setVidrios(int vidrios) {
        this.vidrios = vidrios;
    }

    public String getTransmision() {
        return transmision;
    }

    public void setTransmision(String transmision) {
        this.transmision = transmision;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getTipo() {
        return tipo;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
    

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vehiculo other = (Vehiculo) obj;
        if (!Objects.equals(this.placa, other.placa)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return placa + ", " + precio + ", " + marca + ", " + modelo + ", " + tipo_motor + ", " + año + ", " + recorrido + ", " + color + ", " + tipo_combustible + ", " + vidrios + ", " + transmision + ", " + tipo + ", ";
    }
    
    public String toStringResumido() {
        return "Tipo: " + tipo + "\nPrecio: " + precio + "\nAño: " + año + "\nRecorrido: " + recorrido;
    }

    public static void serializacion(ArrayList<Vehiculo> arr) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("src/vehiculos.ser"))) {
            oos.writeObject(arr);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static ArrayList<Vehiculo> deserializacion() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("src/vehiculos.ser"))) {
            ArrayList<Vehiculo> arr = (ArrayList<Vehiculo>) ois.readObject();
            return arr;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            ArrayList<Vehiculo> arr = new ArrayList<>();
            return arr;
        }

    }
    
    public static ArrayList<String> cargarAno(){
        ArrayList<String> anos = new ArrayList<>();
        ArrayList<Vehiculo> vehiculos = Vehiculo.deserializacion();
        for(Vehiculo v : vehiculos){
            if(!anos.contains(v.año)) anos.add(v.año);
        }
        return anos;
    }

}
