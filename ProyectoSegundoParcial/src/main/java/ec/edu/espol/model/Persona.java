/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;
/**
 *
 * @author jonat
 */
public class Persona implements Serializable {
    
    private static final long serialVersionUID= 78782626363636L;

    protected String nombre;
    protected String apellido;
    protected String correo;
    protected String organizacion;
    protected String usuario;
    protected String clave;
    protected String rol;

    public Persona(String nombre, String apellido, String correo, String organizacion, String usuario, String clave, String rol) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.correo = correo;
        this.organizacion = organizacion;
        this.usuario = usuario;
        this.clave = clave;
        this.rol = rol;
    }
    
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }
    

    @Override
    public String toString() {
        return nombre + "," + apellido + "," + correo + "," + organizacion + "," + usuario + "," + clave + ","+ rol;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Persona other = (Persona) obj;
        if (Objects.equals(this.usuario, other.usuario) || Objects.equals(this.correo, other.correo)) {
            return true;
        }
        return false;
    }

    public static void serializacion(ArrayList<Persona> arr) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("src/personas.ser"))) {
            oos.writeObject(arr);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static ArrayList<Persona> deserializacion() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("src/personas.ser"))) {
            ArrayList<Persona> arr = (ArrayList<Persona>) ois.readObject();
            return arr;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            ArrayList<Persona> arr = new ArrayList<>();
            return arr;
        }

    }

}
