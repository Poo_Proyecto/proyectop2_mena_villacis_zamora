/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author jonat
 */
public class Oferta implements Comparable<Oferta>, Serializable {

    private double valorOferta;
    private String correo;
    private String placa;

    public Oferta(double valorOferta, String correo, String placa) {
        this.valorOferta = valorOferta;
        this.correo = correo;
        this.placa = placa;
    }

    public double getValorOferta() {
        return valorOferta;
    }

    public String getCorreo() {
        return correo;
    }

    public String getPlaca() {
        return placa;
    }

    @Override
    public String toString() {
        return valorOferta + ", " + correo + ", " + placa;
    }
    
    public String toStringResumido() {
        return "Oferta: " + valorOferta + "\nCorreo ofertante" + correo + "\nVehículo ofertado: " + placa;
    }
    
    public static void serializacion(ArrayList<Oferta> arr) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("src/ofertas.ser"))) {
            oos.writeObject(arr);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static ArrayList<Oferta> deserializacion() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("src/ofertas.ser"))) {
            ArrayList<Oferta> arr = (ArrayList<Oferta>) ois.readObject();
            return arr;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            ArrayList<Oferta> arr = new ArrayList<>();
            return arr;
        }

    }

    public static ArrayList<Vehiculo> ftTipo(ArrayList<Vehiculo> lista, String tipo) {
        ArrayList<Vehiculo> vehi = new ArrayList<>();
        for (Vehiculo v : lista) {
            if (v.getTipo().equals(tipo)) {
                vehi.add(v);
            }
        }
        return vehi;
    }

    public static ArrayList<Vehiculo> ftRe(ArrayList<Vehiculo> lista, int minRe, int maxRe) {
        ArrayList<Vehiculo> vehi = new ArrayList<>();
        for (Vehiculo v : lista) {
            if (minRe < v.getRecorrido() && v.getRecorrido() < maxRe) {
                vehi.add(v);
            }
        }
        return vehi;
    }

    public static ArrayList<Vehiculo> ftPrecio(ArrayList<Vehiculo> lista, double precioIni, double precioFin) {
        ArrayList<Vehiculo> vehi = new ArrayList<>();
        for (Vehiculo v : lista) {
            if (precioIni < v.getPrecio() && v.getPrecio() < precioFin) {
                vehi.add(v);
            }
        }
        return vehi;
    }

    public static ArrayList<Vehiculo> ftAno(ArrayList<Vehiculo> lista, int minAno, int maxAno) {
        ArrayList<Vehiculo> vehi = new ArrayList<>();
        for (int i = 0; i < lista.size(); i++) {
            if (minAno < Integer.parseInt(lista.get(i).getAño()) && Integer.parseInt(lista.get(i).getAño()) < maxAno) {
                vehi.add(lista.get(i));
            }
        }
        return vehi;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Oferta other = (Oferta) obj;
        if (!Objects.equals(this.correo, other.correo)) {
            return false;
        }
        if (!Objects.equals(this.placa, other.placa)) {
            return false;
        }
        return true;
    }
    
    public boolean validar() {
        ArrayList<Oferta> ofertas = deserializacion();
        for (Oferta o: ofertas ){
            if (o.equals(this)) return true;
        }
        return false;
    }

    @Override
    public int compareTo(Oferta o) {
        String vp= Double.toString(valorOferta);
        String vpo= Double.toString(o.valorOferta);
        int last= vp.compareTo(vpo);
        return last ;
    }

}
