/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Comprador;
import ec.edu.espol.model.CompradorVendedor;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Vendedor;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author jonat
 */
public class VentanaRegistroController implements Initializable {

    @FXML
    private ComboBox<String> comBoxRol;
    @FXML
    private TextField textNombre;
    @FXML
    private TextField textApellido;
    @FXML
    private TextField textOrganizacion;
    @FXML
    private TextField textCorreo;
    @FXML
    private TextField textUsu;
    @FXML
    private TextField textContra;
    @FXML
    private Button botRegistrar;
    @FXML
    private Button botLimpiar;
    @FXML
    private Button botSalir;

    private ArrayList<TextField> artext;
    private ArrayList<Persona> arrPer;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        artext = new ArrayList<>();
        artext.add(textNombre);
        artext.add(textApellido);
        artext.add(textOrganizacion);
        artext.add(textCorreo);
        artext.add(textUsu);
        artext.add(textContra);
        comBoxRol.setItems(FXCollections.observableArrayList("Vendedor", "Comprador", "Ambos"));
        arrPer = Persona.deserializacion();
        System.out.println(arrPer);
        textNombre.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("([a-zA-Z]*)?")) ? change : null));
        textApellido.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("([a-zA-Z]*)?")) ? change : null));
        textOrganizacion.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("([a-zA-Z]*)?")) ? change : null));
        textUsu.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("([a-zA-Z0-9]*)?")) ? change : null));
        textContra.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("([a-zA-Z0-9]*)?")) ? change : null));
    }

    private boolean validarRegistro() {
        for (Persona p : arrPer) {
            if (p.getCorreo().equals(textCorreo.getText())) {
                return true;
            }
        }
        return false;
    }

    @FXML
    private void registrar(MouseEvent event) {
        String emailPattern = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@"
                + "[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
        Pattern pattern = Pattern.compile(emailPattern);
        if (!validarRegistro()) {
            if (textNombre.getText().equals("") || textApellido.getText().equals("") || textOrganizacion.getText().equals("") || textCorreo.getText().equals("")
                    || textUsu.getText().equals("") || textContra.getText().equals("") || comBoxRol.getValue() == null) {
                Alert a = new Alert(Alert.AlertType.ERROR, "Por favor llene todos los campos");
                a.show();
            } else if (textCorreo.getText() != null) {
                Matcher matcher = pattern.matcher(textCorreo.getText());
                if (!matcher.matches()) {
                    Alert a = new Alert(Alert.AlertType.ERROR, "El formato del email está incorrecto");
                    a.show();
                } else {
                    if (comBoxRol.getValue().equals("Ambos")) {
                        CompradorVendedor perRegistrar = new CompradorVendedor(textNombre.getText(), textApellido.getText(), textCorreo.getText(), textOrganizacion.getText(), textUsu.getText(), textContra.getText(), comBoxRol.getValue());
                        arrPer.add(perRegistrar);
                        Persona.serializacion(arrPer);
                        Alert a = new Alert(Alert.AlertType.CONFIRMATION, "Registro exitoso");
                        a.show();
                    } else if (comBoxRol.getValue().equals("Vendedor")) {
                        Vendedor perRegistrar = new Vendedor(textNombre.getText(), textApellido.getText(), textCorreo.getText(), textOrganizacion.getText(), textUsu.getText(), textContra.getText(), comBoxRol.getValue());
                        arrPer.add(perRegistrar);
                        Persona.serializacion(arrPer);
                        Alert a = new Alert(Alert.AlertType.CONFIRMATION, "Registro exitoso");
                        a.show();
                    } else if (comBoxRol.getValue().equals("Comprador")) {
                        Comprador perRegistrar = new Comprador(textNombre.getText(), textApellido.getText(), textCorreo.getText(), textOrganizacion.getText(), textUsu.getText(), textContra.getText(), comBoxRol.getValue());
                        arrPer.add(perRegistrar);
                        Persona.serializacion(arrPer);
                        Alert a = new Alert(Alert.AlertType.CONFIRMATION, "Registro exitoso");
                        a.show();
                    } else {
                        Alert a = new Alert(Alert.AlertType.ERROR, "Ocurrió un problema con su registro, inténtelo de nuevo");
                        a.show();
                    }
                    try {
                        App.setRoot("VentanaInicio");
                    } catch (IOException ex) {
                        Alert a = new Alert(Alert.AlertType.ERROR, "No se ha encontrado el FXML correspondiente");
                        a.show();
                    }
                }
            } else {
                Alert a = new Alert(Alert.AlertType.ERROR, "Usted ya está registrado");
                a.show();
            }
        }
    }

    @FXML
    private void limpiar(MouseEvent event) {
        for (TextField txt : artext) {
            txt.setText("");
        }
    }

    @FXML
    private void salir(MouseEvent event) {
        Stage stage = (Stage) botSalir.getScene().getWindow();
        stage.close();
    }

}
