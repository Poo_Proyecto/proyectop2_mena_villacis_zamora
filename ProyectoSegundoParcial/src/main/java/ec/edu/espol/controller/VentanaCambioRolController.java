/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Persona;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author a2
 */
public class VentanaCambioRolController implements Initializable {
    
    private String rolantiguo;
    private ArrayList<Persona> arrPer;
    private String nom;
    private String ape;
    private String correo;
    private String org;
    private String usu;
    private String con;
    private String rol;
    private String ventana;
    
    @FXML
    private ComboBox<String> ComboRoles;
    @FXML
    private Button btnGuardarCambioRol;
    @FXML
    private Button btnRegresar;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        arrPer = Persona.deserializacion();
        ComboRoles.setItems(FXCollections.observableArrayList("Vendedor", "Comprador", "Ambos"));
        ventana = "VentanaCambioRol";
    }

    @FXML
    public void recibirParametros4(String textNombre, String textApellido, String texCorreo, String texOrganizacion, String textUsuario, String texContrasena, String textRol, String ventana) {
        this.nom = textNombre;
        this.ape = textApellido;
        this.org = texOrganizacion;
        this.correo = texCorreo;
        this.usu = textUsuario;
        this.con = texContrasena;
        this.rol = textRol;
        this.ventana = ventana;
    }

    @FXML
    private void cambioRol(MouseEvent event) {
        int cont = 0;
        for (int i = 0; i < arrPer.size(); i++) {
            if (correo.equals(arrPer.get(i).getCorreo())) {
                Alert a = new Alert(Alert.AlertType.CONFIRMATION, "¿Seguro que quiere cambiar su rol?");
                a.setHeaderText(null);
                a.setTitle("Información");
                Optional<ButtonType> action = a.showAndWait();
                if (action.get() == ButtonType.OK) {
                    arrPer.get(i).setRol(ComboRoles.getValue());
                    Persona.serializacion(arrPer);
                }
            } else {
                cont = cont + 1;
            }
        }
        if (cont == arrPer.size()) {
            Alert a = new Alert(Alert.AlertType.ERROR, "No se ha podido cambiar rol");
            a.show();
        }
    }

    @FXML
    private void regresar(MouseEvent event) {
        if (!(ComboRoles.getValue() == null)) {
            try {
                FXMLLoader fxmlL = App.loadFXMLoad("VentanaInformacionUsuario");
                App.setRoot(fxmlL);
                VentanaInformacionUsuarioController controller;
                controller = fxmlL.getController();
                controller.recibirParametros2(nom, ape, correo, org, usu, con, ComboRoles.getValue(), ventana);
            } catch (IOException ex) {
                Alert a = new Alert(Alert.AlertType.ERROR, "No se pudo cambiar de ventana");
                a.show();
            }
        } else {
            try {
                FXMLLoader fxmlL = App.loadFXMLoad("VentanaInformacionUsuario");
                App.setRoot(fxmlL);
                VentanaInformacionUsuarioController controller;
                controller = fxmlL.getController();
                controller.recibirParametros2(nom, ape, correo, org, usu, con, rol, ventana);
            } catch (IOException ex) {
                Alert a = new Alert(Alert.AlertType.ERROR, "No se pudo cambiar de ventana");
                a.show();
            }
        }
    }
}
