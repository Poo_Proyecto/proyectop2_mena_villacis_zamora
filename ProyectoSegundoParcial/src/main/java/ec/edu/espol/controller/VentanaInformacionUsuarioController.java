/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author jonat
 */
public class VentanaInformacionUsuarioController implements Initializable {

    @FXML
    private TextField textNombre;
    @FXML
    private TextField textApellido;
    @FXML
    private TextField textCorreo;
    @FXML
    private TextField textOrganizacion;
    @FXML
    private TextField textUsuario;
    @FXML
    private TextField textContrasena;
    @FXML
    private TextField textRol;
    @FXML
    private Button btnCambiarRol;
    @FXML
    private Button btnRegresar;
    @FXML
    private Button btnCambiarContrasena;
    @FXML
    private Button btnSalir;
    private String ventana;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    public void recibirParametros2(String textNombre, String textApellido, String texCorreo, String texOrganizacion, String textUsuario, String texContrasena, String textRol, String ventana) {
        this.textNombre.setText(textNombre);
        this.textApellido.setText(textApellido);
        this.textCorreo.setText(texCorreo);
        this.textOrganizacion.setText(texOrganizacion);
        this.textUsuario.setText(textUsuario);
        this.textContrasena.setText(texContrasena);
        this.textRol.setText(textRol);
        this.ventana = ventana;
    }

    @FXML
    private void regresar(MouseEvent event) {
        if (textRol.getText() != null) {
            if (textRol.getText().equals("Ambos")) {
                try {
                    FXMLLoader fxmlL = App.loadFXMLoad("VentanaOpcionAmbos");
                    App.setRoot(fxmlL);
                    VentanaOpcionAmbosController controller;
                    controller = fxmlL.getController();
                    controller.recibirParametros(textNombre.getText(), textApellido.getText(), textCorreo.getText(), textOrganizacion.getText(), textUsuario.getText(), textContrasena.getText(), textRol.getText());
                } catch (IOException ex) {
                    Alert a = new Alert(Alert.AlertType.ERROR, "No se pudo cambiar de ventana");
                    a.show();
                }
            } else if (textRol.getText().equals("Comprador")) {
                try {
                    FXMLLoader fxmlL = App.loadFXMLoad("VentanaOpcionComprador");
                    App.setRoot(fxmlL);
                    VentanaOpcionCompradorController controller;
                    controller = fxmlL.getController();
                    controller.recibirParametros(textNombre.getText(), textApellido.getText(), textCorreo.getText(), textOrganizacion.getText(), textUsuario.getText(), textContrasena.getText(), textRol.getText());
                } catch (IOException ex) {
                    Alert a = new Alert(Alert.AlertType.ERROR, "No se pudo cambiar de ventana");
                    a.show();
                }
            } else if (textRol.getText().equals("Vendedor"))
            try {
                FXMLLoader fxmlL = App.loadFXMLoad("VentanaOpcionVendedor");
                App.setRoot(fxmlL);
                VentanaOpcionVendedorController controller;
                controller = fxmlL.getController();
                controller.recibirParametros(textNombre.getText(), textApellido.getText(), textCorreo.getText(), textOrganizacion.getText(), textUsuario.getText(), textContrasena.getText(), textRol.getText());
            } catch (IOException ex) {
                Alert a = new Alert(Alert.AlertType.ERROR, "No se pudo cambiar de ventana");
                a.show();
            }
        }
    }

    @FXML
    private void cambiarContrasena(MouseEvent event) {
        try {
            FXMLLoader fxmlL = App.loadFXMLoad("VentanaCambioContra");
            App.setRoot(fxmlL);
            VentanaCambioContraController controller;
            controller = fxmlL.getController();
            controller.recibirParametros5(textNombre.getText(), textApellido.getText(), textCorreo.getText(), textOrganizacion.getText(), textUsuario.getText(), textContrasena.getText(), textRol.getText(), ventana);
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.ERROR, "No se pudo cambiar de ventana");
        }
    }

    @FXML
    private void salir(MouseEvent event) {
        Stage stage = (Stage) btnSalir.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void cambiarRol(MouseEvent event) {
        try {
            FXMLLoader fxmlL = App.loadFXMLoad("VentanaCambioRol");
            App.setRoot(fxmlL);
            VentanaCambioRolController controller;
            controller = fxmlL.getController();
            controller.recibirParametros4(textNombre.getText(), textApellido.getText(), textCorreo.getText(), textOrganizacion.getText(), textUsuario.getText(), textContrasena.getText(), textRol.getText(), ventana);
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.ERROR, "No se pudo cambiar de ventana");
        }
    }
}
