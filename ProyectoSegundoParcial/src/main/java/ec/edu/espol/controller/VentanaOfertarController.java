/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Vehiculo;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author jonat
 */
public class VentanaOfertarController implements Initializable {

    private String ventana;
    private String nom;
    private String ape;
    private String correo;
    private String org;
    private String usu;
    private String con;
    private String rol;
    private ArrayList<Vehiculo> vehiculosTotales;
    private ArrayList<Vehiculo> vehiculos;
    private ArrayList<Oferta> ofertas;

    @FXML
    private VBox vBoxScroll;
    @FXML
    private Button btnRegresar;
    @FXML
    private ComboBox<String> cbxFtTipo;
    @FXML
    private TextField recMin;
    @FXML
    private TextField recMax;
    @FXML
    private TextField minPrecio;
    @FXML
    private TextField maxPrecio;
    @FXML
    private TextField minAno;
    @FXML
    private TextField maxAno;
    @FXML
    private Button btFiltrar;
    @FXML
    private Button btNoFiltrar;
    @FXML
    private VBox vBoxEnviar;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        ofertas = Oferta.deserializacion();
        vehiculosTotales = Vehiculo.deserializacion();
        vehiculos = vehiculosTotales;
        this.cargarVehiculos();
        cbxFtTipo.setItems(FXCollections.observableArrayList("Auto", "Camión", "Camioneta", "Motocicleta"));
        //cbxFtAno.setItems(FXCollections.observableArrayList(Vehiculo.cargarAno()));
    }

    public void cargarVehiculos() {
        for (Vehiculo v : vehiculos) {
            try {
                System.out.println(v.getImagen());
                HBox info = new HBox();
                Image png = new Image(new FileInputStream(v.getImagen()));
                ImageView img = new ImageView(png);
                img.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        vBoxEnviar.getChildren().clear();
                        Button btn = new Button("Enviar Oferta");
                        TextField txt = new TextField();
                        txt.setPromptText("Ingrese Precio");
                        vBoxEnviar.getChildren().addAll(txt, btn);
                        btn.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                            @Override
                            public void handle(MouseEvent event) {

                                if (txt.getText().equals("")) {
                                    Alert a = new Alert(Alert.AlertType.ERROR, "Ingrese el precio");
                                    a.show();
                                } else {
                                    Oferta of = new Oferta(Double.parseDouble(txt.getText()), correo, v.getPlaca());
                                    ofertas.add(of);
                                    Oferta.serializacion(ofertas);
                                    Alert a = new Alert(Alert.AlertType.ERROR, "Su oferta a sido enviada");
                                    a.setHeaderText(null);
                                    a.setTitle("confirmación");
                                    a.show();
                                }
                            }
                        });

                    }
                });
                img.setFitHeight(120);
                img.setPreserveRatio(true);
                Label detalles = new Label(v.toStringResumido());
                detalles.setStyle("-fx-font: 18 arial;");
                info.getChildren().addAll(detalles, img);
                info.setSpacing(10);
                info.setPadding(new Insets(10, 15, 10, 15));
                info.setAlignment(Pos.CENTER);
                vBoxScroll.setAlignment(Pos.CENTER);
                vBoxScroll.getChildren().add(info);
            } catch (FileNotFoundException ex) {
                Alert a = new Alert(Alert.AlertType.INFORMATION, "Hubo un error al cargar los vehículos.");
                a.show();
            }
        }
    }

    public void recibirParametros(String textNombre, String textApellido, String texCorreo, String texOrganizacion, String textUsuario, String texContrasena, String textRol, String ventana) {
        this.nom = textNombre;
        this.ape = textApellido;
        this.org = texOrganizacion;
        this.correo = texCorreo;
        this.usu = textUsuario;
        this.con = texContrasena;
        this.rol = textRol;
        this.ventana = ventana;
    }

    @FXML
    private void regresar(MouseEvent event) {
        try {
            FXMLLoader fxmlL = App.loadFXMLoad("VentanaOpcion" + rol);
            App.setRoot(fxmlL);
            if (rol.equals("Ambos")) {
                VentanaOpcionAmbosController controller;
                controller = fxmlL.getController();
                controller.recibirParametros(nom, ape, correo, org, usu, con, rol);
            } else {
                VentanaOpcionCompradorController controller;
                controller = fxmlL.getController();
                controller.recibirParametros(nom, ape, correo, org, usu, con, rol);
            }
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.ERROR, "No se pudo cambiar de ventana");
            a.show();
        }
    }

    private void flitarTipo() {
        vBoxScroll.getChildren().clear();
        vehiculos = Oferta.ftTipo(vehiculos, cbxFtTipo.getValue());
        this.cargarVehiculos();
    }

    private void flitarAno() {
        vBoxScroll.getChildren().clear();
        vehiculos = Oferta.ftAno(vehiculos, Integer.parseInt(minAno.getText()), Integer.parseInt(maxAno.getText()));
        this.cargarVehiculos();
    }

    private void flitarRec() {
        vBoxScroll.getChildren().clear();
        vehiculos = Oferta.ftRe(vehiculos, Integer.parseInt(recMin.getText()), Integer.parseInt(recMax.getText()));
        this.cargarVehiculos();
    }

    private void flitarPre() {
        vBoxScroll.getChildren().clear();
        vehiculos = Oferta.ftPrecio(vehiculos, Integer.parseInt(minPrecio.getText()), Integer.parseInt(maxPrecio.getText()));
        this.cargarVehiculos();
    }

    @FXML
    private void filtrar(MouseEvent event) {
        this.borrarFiltrado(event);
        if (cbxFtTipo.getValue() != null) {
            this.flitarTipo();
        }
        if (!minAno.getText().equals("") && !maxAno.getText().equals("")) {
            this.flitarAno();
        }
        if (!recMin.getText().equals("") && !recMax.getText().equals("")) {
            this.flitarRec();
        }
        if (!minPrecio.getText().equals("") && !maxPrecio.getText().equals("")) {
            this.flitarPre();
        }
    }

    @FXML
    private void borrarFiltrado(MouseEvent event) {
        vBoxScroll.getChildren().clear();
        vehiculos = vehiculosTotales;
        this.cargarVehiculos();
    }
}
