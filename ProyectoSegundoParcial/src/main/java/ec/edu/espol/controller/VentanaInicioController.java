/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Vehiculo;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author jonat
 */
public class VentanaInicioController implements Initializable {

    @FXML
    private Button botIniciarSesion;
    @FXML
    private Button botRegistro;
    @FXML
    private Button botSalir;
    @FXML
    private ImageView imgvFondo;
    @FXML
    private PasswordField txContrasena;
    private ArrayList<Vehiculo> arrVehi;
    private ArrayList<Persona> arrPer;
    @FXML
    private TextField textCorreo;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        arrPer = Persona.deserializacion();
        System.out.println(arrPer);
        System.out.println(Vehiculo.deserializacion());
        System.out.println(Oferta.deserializacion());
    }

    public Persona validarInicio() {
        for (Persona p : arrPer) {
            if (p.getCorreo().equals(textCorreo.getText()) && p.getClave().equals(txContrasena.getText())) {
                return p;
            }
        }
        return null;
    }

    @FXML
    private void iniciarSesion(MouseEvent event) {
        if (textCorreo.getText().equals("") || txContrasena.getText().equals("")) {
            Alert a = new Alert(Alert.AlertType.INFORMATION, "Por favor llene todos los campos.");
            a.show();
        } else {
            if (this.validarInicio() != null) {
                Persona p = validarInicio();
                String ventanaRol = p.getRol();
                try {
                    FXMLLoader fxmlL = App.loadFXMLoad("VentanaOpcion" + ventanaRol);
                    App.setRoot(fxmlL);
                    VentanaOpcionAmbosController controller;
                    VentanaOpcionVendedorController controller2;
                    VentanaOpcionCompradorController controller3;
                    if (ventanaRol.equals("Ambos")) {
                        controller = fxmlL.getController();
                        String nombres = p.getNombre();
                        String apellidos = p.getApellido();
                        String correo = p.getCorreo();
                        String organizacion = p.getOrganizacion();
                        String usuario = p.getUsuario();
                        String contra = p.getClave();
                        String rol = p.getRol();
                        controller.recibirParametros(nombres, apellidos, correo, organizacion, usuario, contra, rol);
                    } else if (ventanaRol.equals("Vendedor")) {
                        controller2 = fxmlL.getController();
                        String nombres = p.getNombre();
                        String apellidos = p.getApellido();
                        String correo = p.getCorreo();
                        String organizacion = p.getOrganizacion();
                        String usuario = p.getUsuario();
                        String contra = p.getClave();
                        String rol = p.getRol();
                        controller2.recibirParametros(nombres, apellidos, correo, organizacion, usuario, contra, rol);
                    } else if (ventanaRol.equals("Comprador")) {
                        controller3 = fxmlL.getController();
                        String nombres = p.getNombre();
                        String apellidos = p.getApellido();
                        String correo = p.getCorreo();
                        String organizacion = p.getOrganizacion();
                        String usuario = p.getUsuario();
                        String contra = p.getClave();
                        String rol = p.getRol();
                        controller3.recibirParametros(nombres, apellidos, correo, organizacion, usuario, contra, rol);
                    }
                } catch (IOException e) {
                    Alert a = new Alert(Alert.AlertType.ERROR, "Hubo un error al cambiar de escena.");
                    a.show();
                }
            } else {
                Alert a = new Alert(Alert.AlertType.INFORMATION, "El usuario no está registrado.");
                a.show();
            }
        }
    }

    @FXML
    private void registrar(MouseEvent event) {
        try {
            App.setRoot("VentanaRegistro");
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.ERROR, "No se ha encontrado el archivo FXML correspondiente");
            a.show();
        }
    }

    @FXML
    private void salirApp(MouseEvent event) {
        Stage stage = (Stage) botSalir.getScene().getWindow();
        stage.close();
    }

}
