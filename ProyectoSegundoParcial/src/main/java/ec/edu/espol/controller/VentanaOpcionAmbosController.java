/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author jonat
 */
public class VentanaOpcionAmbosController implements Initializable {

    @FXML
    private Button btnRegistrarVehiculo;
    @FXML
    private Button btnAceptarOferta;
    @FXML
    private Button btnOfertar;
    @FXML
    private Button btnPerfil;
    @FXML
    private Button btnRegresar;
    @FXML
    private Button btnSalir;

    private String nom;
    private String ape;
    private String cor;
    private String org;
    private String usu;
    private String con;
    private String rol;
    private String ventana;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ventana = "VentanaOpcionAmbos";
    }

    @FXML
    public void recibirParametros(String textNombre, String textApellido, String texCorreo, String texOrganizacion, String textUsuario, String texContrasena, String textRol) {
        this.nom = textNombre;
        this.ape = textApellido;
        this.cor = texCorreo;
        this.org = texOrganizacion;
        this.usu = textUsuario;
        this.con = texContrasena;
        this.rol = textRol;
    }

    @FXML
    private void registrarVehiculo(MouseEvent event) {
        try {
            FXMLLoader fxmlL = App.loadFXMLoad("VentanaRegistroVehiculo");
            App.setRoot(fxmlL);
            VentanaRegistroVehiculoController controller;
            controller = fxmlL.getController();
            controller.recibirParametroVentana(nom, ape, cor, org, usu, con, rol, ventana);
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.ERROR, "No se pudo cambiar de ventana");
        }
    }

    @FXML
    private void aceptarOferta(MouseEvent event) {
        try {
            FXMLLoader fxmlL = App.loadFXMLoad("VentanaAceptarOferta");
            App.setRoot(fxmlL);
            VentanaAceptarOfertaController controller = fxmlL.getController();
            controller.recibirParametros(nom, ape, cor, org, usu, con, rol);
        } catch (IOException e) {
            Alert a = new Alert(Alert.AlertType.ERROR, "Hubo un error al cambiar de escena.");
            a.show();
        }
    }

    @FXML
    private void ofertar(MouseEvent event) {
        try {
            FXMLLoader fxmlL = App.loadFXMLoad("VentanaOfertar");
            App.setRoot(fxmlL);
            VentanaOfertarController controller = fxmlL.getController();
            controller.recibirParametros(nom, ape, cor, org, usu, con, rol, ventana);
        } catch (IOException e) {
            Alert a = new Alert(Alert.AlertType.ERROR, "Hubo un error al cambiar de escena.");
            a.show();
        }
    }

    @FXML
    private void entrarPerfil(MouseEvent event) {
        try {
            FXMLLoader fxmlL = App.loadFXMLoad("VentanaInformacionUsuario");
            App.setRoot(fxmlL);
            VentanaInformacionUsuarioController controller = fxmlL.getController();
            controller.recibirParametros2(nom, ape, cor, org, usu, con, rol, ventana);
        } catch (IOException e) {
            Alert a = new Alert(Alert.AlertType.ERROR, "Hubo un error al cambiar de escena.");
            a.show();
        }
    }

    @FXML
    private void regresar(MouseEvent event) {
        try {
            App.setRoot("VentanaInicio");
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.ERROR, "No se pudo encontrar el fmxl respectivo");
            a.show();
        }
    }

    @FXML
    private void salir(MouseEvent event) {
        Stage stage = (Stage) btnSalir.getScene().getWindow();
        stage.close();
    }
}
