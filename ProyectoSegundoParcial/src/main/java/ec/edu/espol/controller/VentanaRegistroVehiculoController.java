/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Persona;
import ec.edu.espol.model.Vehiculo;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

/**
 * FXML Controller class
 *
 * @author jonat
 */
public class VentanaRegistroVehiculoController implements Initializable {

    @FXML
    private ComboBox<String> comBoxTipo;
    @FXML
    private TextField textPlaca;
    @FXML
    private TextField textMarca;
    @FXML
    private TextField textModelo;
    @FXML
    private TextField textTipoMotor;
    @FXML
    private TextField textYear;
    @FXML
    private TextField textRecorrido;
    @FXML
    private TextField textColor;
    @FXML
    private TextField textCombustible;
    @FXML
    private TextField textVidrios;
    @FXML
    private ComboBox<String> comBoxTransmision;
    @FXML
    private TextField textPrecio;
    @FXML
    private TextField textRuta;
    @FXML
    private Button btnRegresar;
    @FXML
    private Button btnRegistrar;
    @FXML
    private Button btnSalir;
    @FXML
    private VBox vBox;
    @FXML
    private Button btnAbrir;

    private String ventana;
    private String nom;
    private String ape;
    private String cor;
    private String org;
    private String usu;
    private String con;
    private String rol;
    private ArrayList<Vehiculo> arrVehi;
    private ArrayList<Persona> arrPer;
    private ArrayList<Oferta> arrOfertas;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        arrVehi = Vehiculo.deserializacion();
        arrPer = Persona.deserializacion();
        comBoxTipo.setItems(FXCollections.observableArrayList("Auto", "Camión", "Camioneta", "Motocicleta"));
        comBoxTransmision.setItems(FXCollections.observableArrayList("Automático", "Manual"));
        textPlaca.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("([A-Z]{3}" + "-" + "[0-9]{4})?")) ? change : null));
        textMarca.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("([a-zA-Z]*)?")) ? change : null));
        textModelo.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("([a-zA-Z]*)?")) ? change : null));
        textTipoMotor.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("([a-zA-Z]*)?")) ? change : null));
        textYear.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("([0-9]{4})?")) ? change : null));
        textRecorrido.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("([0-9]*)?")) ? change : null));
        textColor.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("([A-Za-z]*)?")) ? change : null));
        textCombustible.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("([A-Za-z]*)?")) ? change : null));
        textVidrios.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("([0-6]{1})?")) ? change : null));
        textPrecio.setTextFormatter(new TextFormatter<>(change
                -> (change.getControlNewText().matches("([0-9]*" + "." + "[0-9]{2})?")) ? change : null));
    }

    public void recibirParametroVentana(String textNombre, String textApellido, String texCorreo, String texOrganizacion, String textUsuario, String texContrasena, String textRol, String ventana) {
        this.ventana = ventana;
        nom = textNombre;
        ape = textApellido;
        cor = texCorreo;
        org = texOrganizacion;
        usu = textUsuario;
        con = texContrasena;
        rol = textRol;
    }

    @FXML
    private void regresar(MouseEvent event) {
        if (rol != null) {
            if (rol.equals("Ambos")) {
                try {
                    FXMLLoader fxmlL = App.loadFXMLoad("VentanaOpcionAmbos");
                    App.setRoot(fxmlL);
                    VentanaOpcionAmbosController controller;
                    controller = fxmlL.getController();
                    controller.recibirParametros(nom, ape, cor, org, usu, con, rol);
                } catch (IOException ex) {
                    Alert a = new Alert(Alert.AlertType.ERROR, "No se pudo cambiar de ventana");
                    a.show();
                }

            } else if (rol.equals("Vendedor"))
            try {
                FXMLLoader fxmlL = App.loadFXMLoad("VentanaOpcionVendedor");
                App.setRoot(fxmlL);
                VentanaOpcionVendedorController controller;
                controller = fxmlL.getController();
                controller.recibirParametros(nom, ape, cor, org, usu, con, rol);
            } catch (IOException ex) {
                Alert a = new Alert(Alert.AlertType.ERROR, "No se pudo cambiar de ventana");
                a.show();
            }
        }
    }

    @FXML
    private void salir(MouseEvent event) {
        Stage stage = (Stage) btnSalir.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void abrir(ActionEvent event) {
        FileChooser jf = new FileChooser();
        Window window = null;
        jf.showOpenDialog(window);
        File archivo = jf.showOpenDialog(window);
        if (archivo != null) {
            textRuta.setText(archivo.getAbsolutePath());
        }
    }

    private boolean validarRegistro() {
        for (Vehiculo v : arrVehi) {
            if (v.getPlaca().equals(textPlaca.getText())) {
                return true;
            }
        }

        return false;
    }

    @FXML
    private void registrar(MouseEvent event) {
        if (!validarRegistro()) {
            if (comBoxTipo.getValue() == null || textPlaca.getText().equals("") || textMarca.getText().equals("") || textModelo.getText().equals("")
                    || textTipoMotor.getText().equals("") || textYear.getText().equals("") || textRecorrido.getText().equals("") || textColor.getText().equals("")
                    || textCombustible.getText().equals("") || textVidrios.getText().equals("") || comBoxTransmision.getValue() == null || textPrecio.getText().equals("")
                    || textRuta.getText().equals("")) {
                Alert a = new Alert(Alert.AlertType.INFORMATION, "Por favor llene todos los campos.");
                a.show();
            } else if (textPlaca.getText() != null) {
                Vehiculo v = new Vehiculo(textPlaca.getText(), cor, textMarca.getText(), textModelo.getText(), textTipoMotor.getText(), textYear.getText(),
                        Integer.parseInt(textRecorrido.getText()), textColor.getText(), textCombustible.getText(), Integer.parseInt(textVidrios.getText()),
                        comBoxTransmision.getValue(), Double.parseDouble(textPrecio.getText()), comBoxTipo.getValue(), textRuta.getText());
                arrVehi.add(v);
                Vehiculo.serializacion(arrVehi);
                Alert a = new Alert(Alert.AlertType.CONFIRMATION, "Su vehiculo a sido registrado");
                a.show();

            } else {
                Alert a = new Alert(Alert.AlertType.INFORMATION, "El carro no está registrado.");
                a.show();
            }
        }
    }
}
