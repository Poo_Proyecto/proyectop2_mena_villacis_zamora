/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Persona;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

/**
 * FXML Controller class
 *
 * @author a2
 */
public class VentanaCambioContraController implements Initializable {

    @FXML
    private TextField textContraActual;
    @FXML
    private TextField textContraNueva;
    @FXML
    private Button btnGuardar;
    @FXML
    private Button btnRegresar;
    private ArrayList<Persona> arrPer;
    private String ventana;
    private String nom;
    private String ape;
    private String correo;
    private String org;
    private String usu;
    private String con;
    private String rol;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        arrPer = Persona.deserializacion();
        ventana = "VentanaCambioContra";
    }

    @FXML
    public void recibirParametros5(String textNombre, String textApellido, String texCorreo, String texOrganizacion, String textUsuario, String texContrasena, String textRol, String ventana) {
        this.nom = textNombre;
        this.ape = textApellido;
        this.org = texOrganizacion;
        this.correo = texCorreo;
        this.usu = textUsuario;
        this.con = texContrasena;
        this.rol = textRol;
        this.ventana = ventana;
    }

    @FXML
    private void guardar(MouseEvent event) {
        int cont = 0;
        for (int i = 0; i < arrPer.size(); i++) {
            if (textContraActual.getText().equals(arrPer.get(i).getClave())) {
                Alert a = new Alert(Alert.AlertType.CONFIRMATION, "Su contraseña a sido Cambiada");
                a.setHeaderText(null);
                a.setTitle("Confirmación");
                Optional<ButtonType> action = a.showAndWait();
                if (action.get() == ButtonType.OK) {
                    arrPer.get(i).setClave(textContraNueva.getText());
                    Persona.serializacion(arrPer);
                    con = textContraNueva.getText();
                }
            } else {
                cont = cont + 1;
            }
        }
        if (cont == arrPer.size()) {
            Alert a = new Alert(Alert.AlertType.ERROR, "No se ha podido cambiar contraseña, digite bien su contraseña actual");
            a.show();
        }
    }

    @FXML
    private void regresar(MouseEvent event) {
        try {
            FXMLLoader fxmlL = App.loadFXMLoad("VentanaInformacionUsuario");
            App.setRoot(fxmlL);
            VentanaInformacionUsuarioController controller;
            controller = fxmlL.getController();
            controller.recibirParametros2(nom, ape, correo, org, usu, con, rol, ventana);
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.ERROR, "No se pudo cambiar de ventana");
        }
    }

}
