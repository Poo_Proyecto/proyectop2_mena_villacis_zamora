/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Oferta;
import ec.edu.espol.model.Vendedor;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author a2
 */
public class VentanaAceptarOfertaController implements Initializable {
    
    private String nom;
    private String ape;
    private String correo;
    private String org;
    private String usu;
    private String con;
    private String rol;
    private ArrayList<Oferta> ofertas;
    private Button acp;
    private Button recha;
    @FXML
    private HBox hBoxScroll;
    @FXML
    private VBox vboxAcep;
    @FXML
    private Button Regresar;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ofertas = Oferta.deserializacion();
        this.cargarOfertas();
        // TODO
    }
    
    public void recibirParametros(String textNombre, String textApellido, String texCorreo, String texOrganizacion, String textUsuario, String texContrasena, String textRol) {
        this.nom = textNombre;
        this.ape = textApellido;
        this.org = texOrganizacion;
        this.correo = texCorreo;
        this.usu = textUsuario;
        this.con = texContrasena;
        this.rol = textRol;
    }
    
    public void cargarOfertas() {
        for (int i = 0; i < ofertas.size(); i++) {
            Oferta o = ofertas.get(i);
            VBox info = new VBox();
            info.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent event) {
                    vboxAcep.getChildren().clear();
                    acp = new Button("Aceptar Oferta");
                    recha = new Button("Rechazar Oferta");
                    vboxAcep.getChildren().addAll(acp, recha);
                    acp.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            String asunto = "Su oferta ha sido aceptada";
                            String cuerpo = "Felicidades";
                            Vendedor.enviarConGMail(o.getCorreo(), asunto, cuerpo);
                            ofertas.remove(o);
                            Oferta.serializacion(ofertas);
                            hBoxScroll.getChildren().remove(info);
                        }
                    });
                    recha.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
                        @Override
                        public void handle(MouseEvent event) {
                            String asunto = "Su oferta ha sido rechazada";
                            String cuerpo = "F";
                            Vendedor.enviarConGMail(o.getCorreo(), asunto, cuerpo);
                            ofertas.remove(o);
                            Oferta.serializacion(ofertas);
                            hBoxScroll.getChildren().remove(info);
                        }
                    });
                }
            });
            Label detalles = new Label(o.toStringResumido());
            detalles.setStyle("-fx-font: 18 arial;");
            info.getChildren().add(detalles);
            info.setSpacing(10);
            info.setPadding(new Insets(10, 15, 10, 15));
            info.setAlignment(Pos.CENTER);
            hBoxScroll.getChildren().add(info);
        }
        hBoxScroll.setAlignment(Pos.CENTER);
    }
    
        public void botones() {
            hBoxScroll.getChildren().clear();
            this.cargarOfertas();
        }

    @FXML
    private void regresar(MouseEvent event) {
        try {
            FXMLLoader fxmlL = App.loadFXMLoad("VentanaOpcion" + rol);
            App.setRoot(fxmlL);
            if (rol.equals("Ambos")) {
                VentanaOpcionAmbosController controller;
                controller = fxmlL.getController();
                controller.recibirParametros(nom, ape, correo, org, usu, con, rol);
            } else {
                VentanaOpcionVendedorController controller;
                controller = fxmlL.getController();
                controller.recibirParametros(nom, ape, correo, org, usu, con, rol);
            }
        } catch (IOException ex) {
            Alert a = new Alert(Alert.AlertType.ERROR, "No se pudo cambiar de ventana");
            a.show();
        }
    }
}
