module ec.edu.espol.gui {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.base;

    opens ec.edu.espol.gui to javafx.fxml;
    exports ec.edu.espol.gui;

    opens ec.edu.espol.controller to javafx.fxml;
    exports ec.edu.espol.controller;

    opens ec.edu.espol.model to javafx.fxml;
    exports ec.edu.espol.model;
    requires mail;

}
